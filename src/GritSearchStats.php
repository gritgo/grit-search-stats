<?php

namespace WPSearchStats;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Grit\Plugin;
use WP_Query;

class GritSearchStats extends Plugin
{

	protected $search_query = "";

	protected $meta_key = 'search_key_count';

	protected $update_url = 'https://demo.gresak.net/grit-search-stats/index.php';

	protected $current_db_version = "4";

	public function init()
	{
		register_activation_hook($this->plugin_main_file,[$this,'create_database_table']);
		add_action('plugins_loaded',[$this, 'check_update_db']);
	}

	public function setFilters()
	{
		add_action('wp',[$this, 'set_search_query']);
		add_action( 'manage_search-term_posts_columns',[$this, 'add_column_in_search_term_table'], 10, 1 );
		add_action( "manage_search-term_posts_custom_column", [$this, 'display_count_in_search_term_table'],10,2);

	}

	public function set_search_query(  )
	{
		if( is_search()) {
			$search_query = trim(get_search_query());

			if(empty($search_query)) return;

			$query = new WP_Query([
				's' => $search_query,
				'post_type' => 'search-term'
			]);

			if( $query->have_posts()) {

				$post = $query->get_posts()[0];
				$this->add_search_hit( $post->ID );
				return;

			} else {

				$post_id = wp_insert_post([
					'post_title' => $search_query,
					'post_type' => 'search-term',
					'post_status' => 'publish'
				]);

				$this->add_search_hit( $post_id );
			}
		}
	}

	public function add_column_in_search_term_table( $defaults )
	{
		//$last = array_pop( $defaults );
		$defaults[$this->meta_key] = __("Search count",'grit-search-stats');
		//array_push( $defaults, $last);
		return $defaults;
	}

	public function display_count_in_search_term_table($column, $post_id )
	{
		echo get_post_meta( $post_id, $this->meta_key, true );
	}

	public function create_database_table()
	{
		global $wpdb;

		$db_version = get_option('grit_search_stats_db_version');

		if( $this->current_db_version === $db_version) return;

		$table_name = $wpdb->prefix . "grit_search_stats";

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
				id int(10) unsigned NOT NULL AUTO_INCREMENT,
				post_id int(11) NOT NULL,
				time datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
				data text DEFAULT '' NOT NULL,
				PRIMARY KEY (id)
			) $charset_collate;";

		require_once(  ABSPATH . 'wp-admin/includes/upgrade.php' );
		$result = dbDelta($sql);

		update_option( 'grit_search_stats_db_version', $this->current_db_version );
	}

	public function check_update_db()
	{

		if( get_option('grit_search_stats_db_version') != $this->current_db_version ) {
			$this->create_database_table();
		}
	}

	protected function add_search_hit( $post_id )
	{
		global $wpdb;

		$wpdb->insert( $wpdb->prefix . "grit_search_stats", [
			'post_id' => $post_id
		]);

		$current = (int) get_post_meta( $post_id, $this->meta_key, true );
		$current ++;
		update_post_meta( $post_id, $this->meta_key , $current );
	}

}

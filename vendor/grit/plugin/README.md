# Grit Plugin

This composer installable library adds basic boilerplate for WordPress plugins.

## Installation

From within your plugin composer require the library

`composer require grit/plugin`

## Usage

Extend the library with your own class. Use the Grit\Container class as injection container. Your main plugin file could look like this 

```php
//File: example-plugin.php
/**
 * Plugin Name:     Example plugin
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     example
 * Author:          AUTHOR HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     example
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Example
 */

require_once("vendor/autoload.php");

$container = Grit\Container::getInstance();

$container['example'] = new MyNamespace\Example(__FILE__);
```

The main class would look like this

```php
//File: src/Example.php

namespace MyNamespace;

use Grit\Plugin;

class Example extends Plugin
{
    // your overrides and own functions here
}

```

## Reference

Please refer to the docs folder.
<?php 

namespace Grit;

use Carbon_Fields\Carbon_Fields;

class Plugin
{
	/**
	 * Absolute path of the plugin folder
	 * @var string
	 */
	protected $path;

	/**
	 * Url of the plugin folder
	 * @var string
	 */
	protected $baseurl;

	/**
	 * Absolute path of the plugin main file
	 *
	 * @var string
	 */
	protected $plugin_main_file;

	/**
	 * If applicable current post_id
	 * @var bool|int
	 */
	protected $post_id = false;

	/**
	 * Url of the folder containing styles and scripts
	 * @var string
	 */
	protected $assets;

	/**
	 * Absolute path to the templates folder
	 * @var string
	 */
	protected $templates;

	/**
	 * Your text domain 
	 * @var string
	 */
	protected $textdomain;

	/**
	 * Name of plugin directory (used also as plugin slug)
	 * Notably used as directory name for templates override in themes
	 * @var string
	 */
	protected $plugin_dir_name;

	/**
	 * Contains plugin version
	 * @var string
	 */
	protected $version;

	/**
	 * wether to load carbon fields
	 * define as false in extended theme to 
	 * disable loading carbon fields by default
	 * @var boolean
	 */
	protected $load_carbon = true;

	/**
	 * weather to load languages
	 * set to false if you don't want to load languages 
	 * by defauld (not recommended)
	 * @var boolean
	 */
	protected $load_i18n = true;

	protected $update_url = false;

	public function __construct($path)
	{
		$this->plugin_main_file = $path;
		$this->path = trailingslashit(dirname($path));
		$this->baseurl = $this->setBaseUrl();
		$this->plugin_dir_name = $this->setPluginDirName();
		$this->setAssetsUrl("assets");  
		$this->setTemplatesDir("templates");  
		$this->setVersion($path);
		$this->init();
		$this->setFilters();

		add_action('wp',[$this,'setPostId'],10);

		if($this->load_carbon) add_action('after_setup_theme',[$this,'carbon_load']);
		
		if($this->load_i18n) add_action('plugins_loaded',[$this, 'load_textdomain']);

		if($this->update_url) add_action('init',[$this, 'auto_update']);
	}

	/**
	 * Extend this function to define anything
	 * you need to run at instantiation
	 * @return [type] [description]
	 */
	protected function init()
	{}

	/**
	 * Extend this function to define all your filters and actions
	 */
	protected function setFilters()
	{}

	/**
	 * Resolves and loads the template from the child theme, theme or default plugin theme folder
	 * in this order
	 * @param  string $file template file to be loaded
	 * @param  array  $data data to be passed to the file loaded
	 * @return void
	 */
	public function load_template($file, $data = [])
	{

		//child theme
		if(file_exists(get_stylesheet_directory() . "/".$this->plugin_dir_name."/".$file.".php")) {
			
			load_template( get_stylesheet_directory() . "/".$this->plugin_dir_name."/" . $file . ".php", false, $data);
			return;
		}

		//parent theme
		if(file_exists(get_template_directory()."/".$this->plugin_dir_name."/" . $file . ".php")) {

			load_template( get_template_directory() . "/".$this->plugin_dir_name."/" . $file . ".php", false, $data );
			return;

		}

		$path = $this->templates. "/" . $file . ".php";
		if(file_exists($path)){
			load_template( $path , false, $data);
		} else {
			throw new \Exception("Template ". $path . "missing.");
		}
	}

	/**
	 * Resolves the template from the child theme, theme or default plugin theme folder and returns its content 
	 * @param  [type] $file [description]
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
	public function read_template($file, $data = [])
	{
		ob_start();

		$this->load_template($file, $data );

		$content = ob_get_contents();

		ob_end_clean();

		return $content;
	}

	/**
	 * Sets the self::post_id
	 * @hooked wp
	 */
	public function setPostId()
	{
		$post = get_post();
		if(!empty($post)) {
			$this->post_id = $post->ID;
		}
	}

	/**
	 * Load carbon fields. 
	 * If you don't want to load carbon fields by default
	 * define class::load_carbon to false in extended class
	 * @hooked after_setup_theme
	 */
	public function carbon_load()
	{
		Carbon_Fields::boot();
	}

	/**
	 * Load i18n
	 * @hooked plugins_loaded
	 */
	public function load_textdomain()
	{
		if(empty($this->plugin_dir_name)) {
			$this->setPluginDirName();
		}
		load_plugin_textdomain( $this->textdomain, false, $this->plugin_dir_name."/languages" );
	}

	/**
	 * post_id getter
	 * @return int current post id or false
	 */
	public function get_post_id()
	{
		return $this->post_id;
	}

	/**
	 * Use this public function to override templates directory path if needed;
	 * @param string $path path to the new templates directory
	 */
	public function set_template_dir($path)
	{
		$this->templates = $path;
	}


	public function auto_update()
	{
		if( ! function_exists('get_plugins')) {
			require_once ABSPATH . "wp-admin/includes/plugin.php";
		}

		$data = get_file_data($this->plugin_main_file,["version" => "Version", "name" => "Plugin Name"]);
		$version = $data['version'];
		$plugins = get_plugins();
		$plugin = "";
		foreach( $plugins as $key => $value ) {
			if($value['Name'] == $data['name']) {
				$plugin = $key;
				break;
			}
		}
		
		new AutoUpdate($version, $this->update_url, $plugin);
	}


	/**
	 * Sets the plugin_dir_name used also as plugin slug
	 */
	protected function setPluginDirName()
	{
		$segments = explode("/",rtrim($this->path,"/"));
		$this->plugin_dir_name = array_pop($segments);
	}

	/**
	 * Sets the base url of the plugin
	 */
	protected function setBaseUrl()
	{
		$segments = explode("/",trim($this->path,"/"));
		return plugins_url(array_pop($segments));
	}

	/**
	 * Sets the folder where plugin assets reside (styles, scripts, images ...)
	 * @param string $assets assets directory name
	 */
	protected function setAssetsUrl($assets)
	{
		$this->assets = $this->baseurl . "/" .$assets;
	}

	/**
	 * Sets the directory where templates are stored (if applicable)
	 * @param string $templates templates directory name
	 */
	protected function setTemplatesDir($templates)
	{
		$this->templates = $this->path . "/" . $templates;
	}

	/**
	 * Sets the version and textdomain of the plugin based on
	 * metadata in main plugin file
	 * @param string $path plugin absolute path
	 */
	protected function setVersion($path) 
	{
		$wp_version = get_bloginfo( 'version' );
		$data = get_file_data($path,array('Version' => 'Version','Text Domain' => 'Text Domain'));
		$this->version =  $wp_version ."." . $data['Version'];
		$this->textdomain = $data['Text Domain'];
	}

}
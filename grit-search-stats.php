<?php
/*
 Plugin Name: Grit Search Stats
 Plugin URI: https://demo.gresak.net/grit-search-stats
 Description: Tracks and displays terms people search for in your wordpress local search.
 Author: Grit, Gregor Grešak, s.p.
 Author URI: https://gresak.net
 Text Domain: grit-search-stats
 Domain Path: /languages
 Version: 0.3.2

 @package         Grit_Search_Stats
 */

require_once 'vendor/autoload.php';

require_once 'post-types/search-term.php';

$container = Grit\Container::getInstance();

$container ['WPSS'] = new WPSearchStats\GritSearchStats(__FILE__);

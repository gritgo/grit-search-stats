<?php

/**
 * Registers the `search_term` post type.
 */
function search_term_init() {
	register_post_type(
		'search-term',
		[
			'labels'                => [
				'name'                  => __( 'Search terms', 'wpsearchstats' ),
				'singular_name'         => __( 'Search term', 'wpsearchstats' ),
				'all_items'             => __( 'All Search terms', 'wpsearchstats' ),
				'archives'              => __( 'Search term Archives', 'wpsearchstats' ),
				'attributes'            => __( 'Search term Attributes', 'wpsearchstats' ),
				'insert_into_item'      => __( 'Insert into Search term', 'wpsearchstats' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Search term', 'wpsearchstats' ),
				'featured_image'        => _x( 'Featured Image', 'search-term', 'wpsearchstats' ),
				'set_featured_image'    => _x( 'Set featured image', 'search-term', 'wpsearchstats' ),
				'remove_featured_image' => _x( 'Remove featured image', 'search-term', 'wpsearchstats' ),
				'use_featured_image'    => _x( 'Use as featured image', 'search-term', 'wpsearchstats' ),
				'filter_items_list'     => __( 'Filter Search terms list', 'wpsearchstats' ),
				'items_list_navigation' => __( 'Search terms list navigation', 'wpsearchstats' ),
				'items_list'            => __( 'Search terms list', 'wpsearchstats' ),
				'new_item'              => __( 'New Search term', 'wpsearchstats' ),
				'add_new'               => __( 'Add New', 'wpsearchstats' ),
				'add_new_item'          => __( 'Add New Search term', 'wpsearchstats' ),
				'edit_item'             => __( 'Edit Search term', 'wpsearchstats' ),
				'view_item'             => __( 'View Search term', 'wpsearchstats' ),
				'view_items'            => __( 'View Search terms', 'wpsearchstats' ),
				'search_items'          => __( 'Search Search terms', 'wpsearchstats' ),
				'not_found'             => __( 'No Search terms found', 'wpsearchstats' ),
				'not_found_in_trash'    => __( 'No Search terms found in trash', 'wpsearchstats' ),
				'parent_item_colon'     => __( 'Parent Search term:', 'wpsearchstats' ),
				'menu_name'             => __( 'Search terms', 'wpsearchstats' ),
			],
			'public'                => false,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => false,
			'supports'              => [ 'title' ],
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-code-standards',
			'show_in_rest'          => true,
			'rest_base'             => 'search-term',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'search_term_init' );

/**
 * Sets the post updated messages for the `search_term` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `search_term` post type.
 */
function search_term_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['search-term'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Search term updated. <a target="_blank" href="%s">View Search term</a>', 'wpsearchstats' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'wpsearchstats' ),
		3  => __( 'Custom field deleted.', 'wpsearchstats' ),
		4  => __( 'Search term updated.', 'wpsearchstats' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Search term restored to revision from %s', 'wpsearchstats' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Search term published. <a href="%s">View Search term</a>', 'wpsearchstats' ), esc_url( $permalink ) ),
		7  => __( 'Search term saved.', 'wpsearchstats' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Search term submitted. <a target="_blank" href="%s">Preview Search term</a>', 'wpsearchstats' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Search term scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Search term</a>', 'wpsearchstats' ), date_i18n( __( 'M j, Y @ G:i', 'wpsearchstats' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Search term draft updated. <a target="_blank" href="%s">Preview Search term</a>', 'wpsearchstats' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'search_term_updated_messages' );

/**
 * Sets the bulk post updated messages for the `search_term` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `search_term` post type.
 */
function search_term_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['search-term'] = [
		/* translators: %s: Number of Search terms. */
		'updated'   => _n( '%s Search term updated.', '%s Search terms updated.', $bulk_counts['updated'], 'wpsearchstats' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Search term not updated, somebody is editing it.', 'wpsearchstats' ) :
						/* translators: %s: Number of Search terms. */
						_n( '%s Search term not updated, somebody is editing it.', '%s Search terms not updated, somebody is editing them.', $bulk_counts['locked'], 'wpsearchstats' ),
		/* translators: %s: Number of Search terms. */
		'deleted'   => _n( '%s Search term permanently deleted.', '%s Search terms permanently deleted.', $bulk_counts['deleted'], 'wpsearchstats' ),
		/* translators: %s: Number of Search terms. */
		'trashed'   => _n( '%s Search term moved to the Trash.', '%s Search terms moved to the Trash.', $bulk_counts['trashed'], 'wpsearchstats' ),
		/* translators: %s: Number of Search terms. */
		'untrashed' => _n( '%s Search term restored from the Trash.', '%s Search terms restored from the Trash.', $bulk_counts['untrashed'], 'wpsearchstats' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'search_term_bulk_updated_messages', 10, 2 );
